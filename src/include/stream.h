#ifndef STREAM_H
#define STREAM_H

#include <stdlib.h>
#include <string.h>

typedef struct stream_t {
  u_int8_t *buff;
  size_t pos;
} stream;

u_int8_t read_uint8(stream *s);
u_int16_t read_uint16(stream *s);
u_int32_t read_uint32(stream *s);

void read_unsigned_byte_arr(stream *s, u_int8_t *arr, size_t size);
void read_byte_arr(stream *s, int8_t *arr, size_t size);

void write_uint8(stream *s, u_int8_t val);
void write_uint16(stream *s, u_int16_t val);
void write_uint32(stream *s, u_int32_t val);

void write_unsigned_byte_arr(stream *s, u_int8_t *arr, size_t size);
void write_byte_arr(stream *s, int8_t *arr, size_t size);

stream *construct_stream(u_int8_t *buff, size_t pos);

#endif
