#ifndef CONFIG_TYPES_H
#define CONFIG_TYPES_H

#include "options.h"

typedef struct allocation_t {
  u_int8_t hardware_addr[6];
  u_int8_t ip_address[4];
} allocation;

typedef struct __attribute__((__packed__)) {
  u_int8_t hardware_addr[6];
  option *options;
  int len;
} local_option;

#endif
