#include "threading.h"

void *handle_request(void *request_args_void) {
  request_args *args = (request_args *) request_args_void;

  dhcp_packet packet = read_dhcp_packet(args->buffer, args->len);
  dhcp_packet response = respond(packet);

  u_int8_t *data = write_dhcp_packet(response);
  send_response(data, response.size, args->client_ip);

  free_options(packet.options);
  free_options(response.options);

  free(args->buffer);
  free(args);

  pthread_exit(NULL);
}

void create_thread_for_request(size_t len, u_int8_t *buffer, struct in_addr client_ip) {
  request_args *args = (request_args *) malloc(sizeof(request_args));
  args->len = len;
  args->buffer = buffer;
  args->client_ip = client_ip;

  pthread_t tid;
  pthread_create(&tid, NULL, handle_request, args);
}
