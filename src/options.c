#include "options.h"

#define OPTIONS_SIZE 256

option **read_options(u_int8_t *arr, size_t size) {
  stream s;
  s.buff = arr;
  s.pos = 0;
  
  option **option_arr = construct_option_arr();

  while (s.pos < size) {
    u_int8_t option_code = read_uint8(&s);
    
    if (option_code == 255) {
      break;
    }

    option *curr = (option *) malloc(sizeof(option));
    curr->code = option_code;

    u_int8_t len = read_uint8(&s);
    curr->len = len;

    u_int8_t *data = (u_int8_t *) malloc((len) * sizeof(u_int8_t));
    read_unsigned_byte_arr(&s, data, len);
    curr->data = data;

    option_arr[option_code] = curr;
  }
  
  return option_arr;
}

u_int8_t *write_options(option **option_arr) {
  size_t len = size_of_options(option_arr);
  len += 1;
  
  u_int8_t *buff = (u_int8_t *) malloc(sizeof(u_int8_t) * len);
  stream s;
  s.buff = buff;
  s.pos = 0;

  for (int i = 0; i < OPTIONS_SIZE; i++) {
    if (option_arr[i] != 0) {
      write_uint8(&s, option_arr[i]->code);
      write_uint8(&s, option_arr[i]->len);
      write_unsigned_byte_arr(&s, option_arr[i]->data, option_arr[i]->len);
    }
  }
  
  write_uint8(&s, 255);

  return buff;
}

option **construct_option_arr(void) {
  option **option_arr = malloc(OPTIONS_SIZE * sizeof(option *));
  memset(option_arr, 0, (OPTIONS_SIZE * sizeof(option *)));
  
  return option_arr;
}

void construct_option(option **option_arr, u_int8_t option_code, u_int8_t len, u_int8_t *data) {
  option *new = (option *) malloc(sizeof(option));
  new->code = option_code;
  new->len = len;

  u_int8_t *temp_data = (u_int8_t *) malloc(sizeof(u_int8_t) * len);
  memcpy(temp_data, data, len);
  new->data = temp_data;

  option_arr[option_code] = new;
}

void construct_option_with_str(option **option_arr, u_int8_t option_code, char *str) {
  construct_option(option_arr, option_code, (u_int8_t) strlen(str) + 1, (u_int8_t *) str);
}

void construct_option_with_u8(option **option_arr, u_int8_t option_code, u_int8_t val) {
  construct_option(option_arr, option_code, 1, &val);
}

void free_options(option **options) {
  for (int i = 0; i < OPTIONS_SIZE; i++) {
    if (options[i] != 0) {
      free(options[i]->data);
      free(options[i]);
    }
  }

  free(options);
}

size_t size_of_options(option **option_arr) {
  size_t total = 0;

  for (int i = 0; i < OPTIONS_SIZE; i++) {
    if (option_arr[i] != 0) {
      total += sizeof(option_arr[i]->code);
      total += sizeof(option_arr[i]->len);
      total += option_arr[i]->len;
    }
  }
  
  total += 1; // For end option

  return total;
}

