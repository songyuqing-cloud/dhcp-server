#ifndef CONFIG_H
#define CONFIG_H

#include "src/include/options.h"
#include "src/include/config_types.h"

static u_int8_t server_identifier[] = {127, 0, 0, 1}; // Temporary. TODO Get host IP dynamically.

static allocation allocations[] = {
  {{0x34, 0xe6, 0xd7, 0x0f, 0xa9, 0x83}, {192, 168, 1, 235}},
  {{0xde, 0xad, 0xc0, 0xde, 0xca, 0xfe}, {192, 168, 1, 236}},
};

static option global_options[] = {
  {1, 4, (u_int8_t[]) {0xFF, 0xFF, 0xFF, 0x00}},
  {3, 8, (u_int8_t[]) {192, 168, 1, 1, 192, 168, 1, 2}}
};

static option local_options[] = {
  {1, 4, (u_int8_t[]) {0xFF, 0x00, 0xFF, 0x00}},
  {3, 4, (u_int8_t[]) {192, 168, 1, 253}}
};

static local_option local_options_map[] = {
  {{0xde, 0xad, 0xc0, 0xde, 0xca, 0xfe}, local_options, 2},
};

#endif
